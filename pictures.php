<?php
// Connect to database
$connection = mysqli_connect('localhost', 'root', '', 'rest_api');

$request_method = $_SERVER["REQUEST_METHOD"];
switch ($request_method) {
    case 'GET':
// Retrive picture
        if (!empty($_GET["picture_id"])) {
            $picture_id = intval($_GET["picture_id"]);
            get_pictures($picture_id);
        } else {
            get_pictures();
        }
        break;
    case 'POST':
// Insert picture
        insert_picture();
        break;
    case 'PUT':
// Update picture
        $picture_id = intval($_GET["picture_id"]);
        update_picture($picture_id);
        break;
    case 'DELETE':
// Delete picture
        $picture_id = intval($_GET["picture_id"]);
        delete_picture($picture_id);
        break;
    default:
// Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}

function insert_picture()
{
    global $connection;
    $picture_name = $_POST["picture_name"];
    $query = "INSERT INTO pictures SET picture_name='{$picture_name}'";
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Picture Added Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Picture Addition Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function get_pictures($picture_id = 0)
{
    global $connection;
    $query = "SELECT * FROM pictures";
    if ($picture_id != 0) {
        $query .= " WHERE id=" . $picture_id . " LIMIT 1";
    }
    $response = array();
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_array($result)) {
        $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function delete_picture($picture_id)
{
    global $connection;
    $query = "DELETE FROM pictures WHERE id=" . $picture_id;
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Picture Deleted Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Picture Deletion Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function update_picture($picture_id)
{
    global $connection;
    parse_str(file_get_contents("php://input"), $post_vars);
    $picture_name = $post_vars["picture_name"];
    $picture_path = $post_vars["picture_path"];
    $picture_thumb = $post_vars["picture_thumb"];
    $query = "UPDATE pictures SET picture_name='{$picture_name}', picture_path='{$picture_path}', picture_thumb='{$picture_thumb}' WHERE id=" . $picture_id;
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Picture Updated Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Picture Updation Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

// Close database connection
mysqli_close($connection);
?>