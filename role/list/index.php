<?php
$roles = array(
    'guest' => array('login'),
    'normal' => array('logout', 'view_content'),
    'editor' => array('logout', 'view_content', 'manage_content')
);
echo json_encode($roles);
?>
