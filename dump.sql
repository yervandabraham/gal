/*
SQLyog Community v12.2.4 (64 bit)
MySQL - 10.1.22-MariaDB : Database - rest_api
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rest_api` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rest_api`;

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `gallery` */

insert  into `gallery`(`id`,`gallery_name`) values (1,'gallery1');
insert  into `gallery`(`id`,`gallery_name`) values (2,'gallery2');

/*Table structure for table `picture_gallery` */

DROP TABLE IF EXISTS `picture_gallery`;

CREATE TABLE `picture_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_id` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `picture_gallery` */

insert  into `picture_gallery`(`id`,`picture_id`,`gallery_id`) values (1,1,1);
insert  into `picture_gallery`(`id`,`picture_id`,`gallery_id`) values (2,2,1);
insert  into `picture_gallery`(`id`,`picture_id`,`gallery_id`) values (3,3,1);
insert  into `picture_gallery`(`id`,`picture_id`,`gallery_id`) values (4,4,2);
insert  into `picture_gallery`(`id`,`picture_id`,`gallery_id`) values (5,5,2);

/*Table structure for table `pictures` */

DROP TABLE IF EXISTS `pictures`;

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_name` varchar(255) DEFAULT NULL,
  `picture_path` varchar(255) DEFAULT NULL,
  `picture_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pictures` */

insert  into `pictures`(`id`,`picture_name`,`picture_path`,`picture_thumb`) values (1,'picture1','http://37.252.66.69/gal/img/gallery/1.jpg','http://37.252.66.69/gal/img/gallery/1s.jpg');
insert  into `pictures`(`id`,`picture_name`,`picture_path`,`picture_thumb`) values (2,'picture2','http://37.252.66.69/gal/img/gallery/2.jpg','http://37.252.66.69/gal/img/gallery/2s.jpg');
insert  into `pictures`(`id`,`picture_name`,`picture_path`,`picture_thumb`) values (3,'picture3','http://37.252.66.69/gal/img/gallery/3.jpg','http://37.252.66.69/gal/img/gallery/3s.jpg');
insert  into `pictures`(`id`,`picture_name`,`picture_path`,`picture_thumb`) values (4,'picture4','http://37.252.66.69/gal/img/gallery/4.jpg','http://37.252.66.69/gal/img/gallery/4s.jpg');
insert  into `pictures`(`id`,`picture_name`,`picture_path`,`picture_thumb`) values (5,'picture5','http://37.252.66.69/gal/img/gallery/5.jpg','http://37.252.66.69/gal/img/gallery/5s.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
