<?php
// Connect to database
$connection = mysqli_connect('localhost', 'root', '', 'rest_api');

$request_method = $_SERVER["REQUEST_METHOD"];
switch ($request_method) {
    case 'GET':
// Retrive gallery
        if (!empty($_GET["gallery_id"])) {
            $gallery_id = intval($_GET["gallery_id"]);
            get_galleries($gallery_id);
        } else {
            get_galleries();
        }
        break;
    case 'POST':
// Insert gallery
        insert_gallery();
        break;
    case 'PUT':
// Update gallery
        $gallery_id = intval($_GET["gallery_id"]);
        update_gallery($gallery_id);
        break;
    case 'DELETE':
// Delete gallery
        $gallery_id = intval($_GET["gallery_id"]);
        delete_gallery($gallery_id);
        break;
    default:
// Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}

function insert_gallery()
{
    global $connection;
    $gallery_name = $_POST["gallery_name"];
    $query = "INSERT INTO gallery SET gallery_name='{$gallery_name}'";
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Gallery Added Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Gallery Addition Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function get_galleries($gallery_id = 0)
{
    global $connection;
    $query = "SELECT * FROM gallery";
    if ($gallery_id != 0) {
        $query .= " WHERE id=" . $gallery_id . " LIMIT 1";
    }
    $response = array();
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_array($result)) {
        $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function delete_gallery($gallery_id)
{
    global $connection;
    $query = "DELETE FROM gallerys WHERE id=" . $gallery_id;
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Gallery Deleted Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Gallery Deletion Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

function update_gallery($gallery_id)
{
    global $connection;
    parse_str(file_get_contents("php://input"), $post_vars);
    $gallery_name = $post_vars["gallery_name"];
    $query = "UPDATE gallery SET gallery_name='{$gallery_name}' WHERE id=" . $gallery_id;
    if (mysqli_query($connection, $query)) {
        $response = array(
            'status' => 1,
            'status_message' => 'Gallery Updated Successfully.'
        );
    } else {
        $response = array(
            'status' => 0,
            'status_message' => 'Gallery Updation Failed.'
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

// Close database connection
mysqli_close($connection);
?>